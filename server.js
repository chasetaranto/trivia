var express = require('express');
var http = require('http');
var io = require('socket.io');
var app = express();
app.use(express.static('./public'));
var server =http.createServer(app).listen(80);
io = io.listen(server); 
 
var numUsers = 0;
var rediedUsers = 0;
var users = [];
var scores = [];
var curQ;


//temp variables
var questionNum = 0;

var allUsers = [];
//called when a client connects
io.sockets.on("connection",function(socket){

    function nextQuestion(){
    switch(questionNum){
      case 0:
        curQ = {"q":"How many bits long is IPV4","a":"32","b":"64","c":"256","d":"4","answer":"a"};
        break;
      case 1:
        curQ = {"q":"How many layers are in the OSI model","a":"5","b":"12","c":"6","d":"7","answer":"d"};
        break;
      case 2:
        curQ = {"q":"How much wood could a woodchuck chuck","a":"32","b":"none",
        "c":"a woodchuck could chuck as much wood as a woodchuck could chuck if a woodchuck could chuck wood",
        "d":"-1","answer":"c"};
        break;
      case 3:
        curQ = {"q":"What's the answer to life","a":"an apple a day","b":"42","c":"I don't know","d":"I don't care",
        "answer":"b"};
        break;
      case 4:
        curQ = {"q":"This is kinda neat right","a":"no","b":"YES!!","c":"nah","d":"whatever","answer":"b"};
        break;
      default:
        curQ = {"q":"All out of Questions","a":"","b":"","c":"","d":"","answer":""};
    }
    socket.broadcast.emit("q",curQ);
    socket.emit("q",curQ);
    questionNum++;
  }


  //called when a player joins a game
  socket.on("join",function(data){
    socket.emit("reply",{"players":users,"scores":scores,"num":numUsers});
  });

  //called when a player is added to the game
  socket.on("added",function(data){
    allUsers.push(socket);
    console.log(data["name"]+" has joined.");
    users.push(data["name"]);
    scores.push(0);
    numUsers = numUsers+1;
    socket.broadcast.emit("added",{"list":users,"scores":scores,"num":numUsers});
  });

  //called when player submits an answer
  socket.on("answer",function(data){
    console.log(data["name"]+" chose "+data["answer"]);
    if(curQ != null){
      if(curQ["answer"] == data["answer"]){
        console.log(data["name"]+" chose the correct answer");
        socket.emit("feedback",{"righness":"correct"});
        scores[users.indexOf(data["name"])]++;
        socket.broadcast.emit("refresh",{});
      }
      else{
        socket.emit("feedback",{"righness":"incorrect"});
      }
      //rediedUsers++;
      if(rediedUsers != 0 && rediedUsers == numUsers){
        console.log("all ready");
        nextQuestion();
        rediedUsers = 0;
      }
    }
  });

  //called when player redies up
  socket.on("ready",function(data){
    rediedUsers++;
    var i = allUsers.indexOf(socket);
    console.log(users[i]+" has redied up");
    console.log(rediedUsers+" / "+numUsers);
    if(rediedUsers != 0 && rediedUsers == numUsers){
      console.log("all ready");
      nextQuestion();
      rediedUsers = 0;
    }
  });

  //called when the player quits the game
  socket.on("disconnect",function(data){
    var i = allUsers.indexOf(socket);
    if(i > -1){
      //console.log(socket.id);
      allUsers.splice(i,1);
      users.splice(i,1);
      scores.splice(i,1);
      numUsers = numUsers - 1;
      socket.broadcast.emit("refresh",{});
    }
  });
});